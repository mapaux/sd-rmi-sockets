import java.*;

public class Sensor {
	
	private String tipo;
	private int valor;
	private long fecha;
	
	
	public Sensor() 
	{
		this.tipo = null;
		this.valor = 15;
		this.fecha = System.nanoTime();
	}
	
	public Sensor(String tipo_ent, int dato){
		this.tipo = tipo_ent;
		this.valor = dato;
		this.fecha = System.nanoTime();
	}
	
	public String getTipo(){
		return this.tipo;
	}
	
	public void setTipo(String tipo_ent){
		this.tipo = tipo_ent;
	}
	
	public int getValor(){
		return this.valor;
	}
	
	public void setValor(int dato){
		this.valor = dato;
	}
	
	
	
	
	
	
}