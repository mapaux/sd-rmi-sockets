import java.lang.Exception;
import java.net.Socket;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.io.*;

public class hiloServidor extends Thread {

	private Socket skCliente;
	
	private BufferedReader socketInput;
	private PrintWriter socketOutput;
	
	public hiloServidor(Socket p_cliente)
	{
		this.skCliente = p_cliente;
	}
	
	private void establecerFlujoEntrada(Socket socket) throws IOException{
		InputStream flujoEntrada = socket.getInputStream();
		socketInput = new BufferedReader(new InputStreamReader(flujoEntrada));
	}
	
	private void establecerFlujoSalida(Socket socket) throws IOException{
		OutputStream flujoSalida = socket.getOutputStream();
		socketOutput = new PrintWriter(new OutputStreamWriter(flujoSalida));
	}
	
	private void establecerFlujos(Socket socket) throws IOException{
		establecerFlujoEntrada(socket);
		establecerFlujoSalida(socket);
	}
	
	/*
	* Lee datos del socket. Supone que se le pasa un buffer con hueco 
	*	suficiente para los datos. Devuelve el numero de bytes leidos o
	* 0 si se cierra fichero o -1 si hay error.
	*/
	public String leeSocket() throws IOException
	{
		String mensaje = "";
		
		System.out.println("Esperando leer");
			
		mensaje = socketInput.readLine();
			
		System.out.println("Mensaje recibido");
			
		System.out.println("\t" + mensaje);					
		
      return mensaje;
	}

	/*
	* Escribe dato en el socket cliente. Devuelve numero de bytes escritos,
	* o -1 si hay error.
	*/
	public void escribeSocket (String p_Datos) throws IOException
	{
		socketOutput.println(p_Datos);
		socketOutput.flush();
		
	}
	
    public void run() {
		String Cadena="";
		
        try {
			establecerFlujos(skCliente);
			Cadena = this.leeSocket();
			
			Socket skControlador = new Socket("localhost", 9998);
			
			establecerFlujos(skControlador);
				System.out.println("Mando la cadena al controlador");
				this.escribeSocket(Cadena);						
				
				System.out.println("Recibo la cadena del controlador");
				Cadena = this.leeSocket();
				
			establecerFlujos(skCliente);
			
				System.out.println("Escribo la cadena recibida del controlador en la página");
				
				String method = "";
				
				if(Cadena.equals("fallo_parametro")){
					method = "406 Not Acceptable";
					this.escribeSocket("HTTP1.1 " + method);
				}
				else if(Cadena.equals("metodo_post")){
					method = "405 Not Allowed";
					this.escribeSocket("HTTP/1.1 " + method + "\r\n\r\n<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n" +
							"<TITLE>" + method + "</TITLE>\r\n</HEAD><BODY>\r\n" + 
			                   "<H1>Method Not Allowed</H1>\r\n Método no soportado <P>\r\n</BODY></HTML>\r\n");
				}
				else if(Cadena.equals("not_found")){
					method = "404 Not Found";
					this.escribeSocket("HTTP/1.1 " + method + "\r\n\r\n<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n" +
							"<TITLE>" + method + "</TITLE>\r\n</HEAD><BODY>\r\n" + 
			                   "<H1>" + method + "</H1>\r\n Recurso no encontrado <P>\r\n</BODY></HTML>\r\n");
				}
				else{
					method = "200 OK";
					String partir[], tipo = "";
					partir = Cadena.split(",");
					if(partir[1].equals("1")){
						tipo = "temperatura";
					}
					else
						tipo = "humedad";
					
					DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					Date date = new Date();
					String fecha = dateFormat.format(date);
					
					this.escribeSocket("HTTP/1.1 " + method + " \r\n\r\n<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n" + 
						   "<TITLE>" + method + "</TITLE>\r\n" + 
		                   "</HEAD><BODY>\r\n" + 
		                   "<H1> Peticion correcta </H1>\r\n Invernadero " + partir[0] + " sensor de " + tipo + " con valor " + partir[2] + " y en la fecha " + fecha + " <P>\r\n" + 
		                   "</BODY></HTML>\r\n");
				}
				//this.escribeSocket(Cadena);
			
			skCliente.close();							
        }
        catch (Exception e) {
          System.out.println("Error: " + e.toString());
        }
      }
}
