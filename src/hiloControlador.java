import java.lang.Exception;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.io.*;

public class hiloControlador extends Thread {

	private Socket skCliente;
	
	private BufferedReader socketInput;
	private PrintWriter socketOutput;
	
	public hiloControlador(Socket p_cliente)
	{
		this.skCliente = p_cliente;
	}
	
	private void establecerFlujoEntrada(Socket socket) throws IOException{
		InputStream flujoEntrada = socket.getInputStream();
		socketInput = new BufferedReader(new InputStreamReader(flujoEntrada));
	}
	
	private void establecerFlujoSalida(Socket socket) throws IOException{
		OutputStream flujoSalida = socket.getOutputStream();
		socketOutput = new PrintWriter(new OutputStreamWriter(flujoSalida));
	}
	
	private void establecerFlujos(Socket socket) throws IOException{
		establecerFlujoEntrada(socket);
		establecerFlujoSalida(socket);
	}
	
	/*
	* Lee datos del socket. Supone que se le pasa un buffer con hueco 
	*	suficiente para los datos. Devuelve el numero de bytes leidos o
	* 0 si se cierra fichero o -1 si hay error.
	*/
	public String leeSocket() throws IOException
	{
		String mensaje = "";
		
		System.out.println("Esperando leer");
			
		mensaje = socketInput.readLine();
			
		System.out.println("Mensaje recibido");
			
		System.out.println("\t" + mensaje);					
		
      return mensaje;
	}

	/*
	* Escribe dato en el socket cliente. Devuelve numero de bytes escritos,
	* o -1 si hay error.
	*/
	public void escribeSocket (String p_Datos) throws IOException
	{
		socketOutput.println(p_Datos);
		socketOutput.flush();
		
	}
	
	public void elegirOperacion(String cadenaRecibida) throws IOException, NotBoundException{
		String metodo = "", url = "", aux2 = "", datos[];
		
		datos = cadenaRecibida.split(" ");
		
		metodo = datos[0];
		
		if(metodo.equals("GET")){
			aux2 = datos[1];
			
			url = aux2.substring(1, aux2.length());
			
			seleccionarAccion(url);
		}
		else{
			this.escribeSocket("metodo_post");
			//("HTTP1.1 405 Method Not Allowed")
			//System.out.println("DECIR QUE EL METODO NO ESTA SOPORTADO");			
		}			
	}
	
	public void seleccionarAccion(String cadenaRecibida) throws IOException, NotBoundException{
		String accion = "", invernadero = "", sensor = "", direccion = "", datos[], aux[];
		
		InterfazRemoto objetoRemoto = null;
		
		datos = cadenaRecibida.split("\\?");
		
		accion = datos[0];
		
		invernadero = datos[1];
		
		direccion = "rmi://localhost:1099/" + invernadero;
		
		if ((accion.equals("obtenerTemperatura")) || (accion.equals("obtenerHumedad"))){
			if(datos.length == 3){
				
				try{
					System.setSecurityManager(new RMISecurityManager());
					objetoRemoto = (InterfazRemoto) Naming.lookup(direccion);
				}
				catch(Exception ex)
	            {
	                System.out.println("Error al instanciar el objeto remoto "+ex);
	                System.exit(0);
	            }
				
				aux = invernadero.split("=");
				
				int dato = 0, tipo = 0;
				
				if(datos[2].equals("sensorTemperatura")){
					dato = objetoRemoto.getTempSensor();
					tipo = 1;
				}
				else if(datos[2].equals("sensorHumedad")){
					dato = objetoRemoto.getHumSensor();
					tipo = 2;
				}
				else{
					this.escribeSocket("fallo_parametro");
				}
				
				this.escribeSocket(aux[1] + "," + tipo + "," + Integer.toString(dato));
				System.out.println("He devuelto el texto");
				
			}
			else{
				//FORMATO INCORRECTO
				this.escribeSocket("fallo_parametro");
				System.out.println("CANTIDAD INCORRECTA DE PARAMETROS");
			}
		}
		else if ((accion.equals("regularTemperatura")) || (accion.equals("regularHumedad"))){
			if(datos.length == 2){
				
				try{
					System.setSecurityManager(new RMISecurityManager());
					objetoRemoto = (InterfazRemoto) Naming.lookup(direccion);
				}
				catch(Exception ex)
	            {
					this.escribeSocket("not_found");
					System.out.println("Error al instanciar el objeto remoto "+ex);
	                System.exit(0);
	            }
				
				int dato = 0, dato2 = 0, tipo = 0;
				String cambio = "";
				
				aux = invernadero.split("=");
				
				if (accion.equals("regularTemperatura")){
					dato = objetoRemoto.getTempSensor();
					objetoRemoto.setTempSensor();
					dato2 =  objetoRemoto.getTempSensor();
					if(dato < dato2){
						cambio = "Calefactor activado";
					}
					else if(dato > dato2){
						cambio = "Sistema de ventilacion activado";
					}
					else
						cambio = "Todo sigue igual";
					tipo = 1;
				}
				else{
					dato = objetoRemoto.getHumSensor();
					objetoRemoto.setHumSensor();
					dato2 = objetoRemoto.getHumSensor();
					
					if(dato < dato2){
						cambio = "Goteo activado";
					}
					else if(dato > dato2){
						cambio = "Deshumidificador activado";
					}
					else
						cambio = "Todo sigue igual";
					tipo = 2;
				}
				
	            Naming.rebind (direccion, objetoRemoto);
				
	            this.escribeSocket(aux[1] + "," + tipo + "," + Integer.toString(dato2) + " " + cambio);
	            
			}
			else{
				this.escribeSocket("fallo_parametro");
				System.out.println("CANTIDAD INCORRECTA DE PARAMETROS");
			}
		}
		else{
			//MOSTRAR ACCION INCORRECTA
		}
			
		
	}
	
    public void run() {
		String Cadena="";
		
        try {
        	System.out.println("Hola, soy tu controlador");
        	
        	establecerFlujos(skCliente);
			
        	Cadena = this.leeSocket();
			
			elegirOperacion(Cadena);
			
			//this.escribeSocket(Cadena);				
				
			skCliente.close();
        }
        catch (Exception e) {
          System.out.println("Error: " + e.toString());
        }
      }
}
