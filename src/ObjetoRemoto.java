import java.io.Serializable; 
import java.rmi.*;
import java.rmi.server.*;


public class ObjetoRemoto extends UnicastRemoteObject
implements InterfazRemoto, Serializable 
{     
	protected ObjetoRemoto() throws RemoteException {
		super();
		this.s_hum = new Sensor();
		this.s_temp = new Sensor();
		this.actu = new actuador();
		// TODO Auto-generated constructor stub
	}

	private Sensor s_temp;
	private Sensor s_hum;
	private actuador actu;
	
	public void ObjetoRemoto () throws RemoteException 
	{
		this.s_hum = new Sensor();
		this.s_temp = new Sensor();
		this.actu = new actuador();
	}
	
	public void ObjetoRemoto(Sensor s_h, Sensor s_t, actuador act)  throws RemoteException 
	{
		this.s_hum = s_h;
		this.s_temp = s_t;
		this.actu = act;
	}
	
	public int getTempSensor () throws RemoteException{
		return s_temp.getValor();
	}

	public int getHumSensor() throws RemoteException {
		return s_hum.getValor();
	}
	
	public void setTempSensor () throws RemoteException{
		this.s_temp = this.actu.regularTemp(s_temp);
	}
	
	public void setHumSensor () throws RemoteException{
		this.s_hum = this.actu.regularHum(s_hum);
	}
	
}
