import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfazRemoto extends Remote {
    
    public void ObjetoRemoto () throws RemoteException;
    public void ObjetoRemoto(Sensor s_h, Sensor s_t, actuador act)  throws RemoteException;
    public int getTempSensor () throws RemoteException;
    public int getHumSensor () throws RemoteException;
    public void setTempSensor () throws RemoteException;
	public void setHumSensor () throws RemoteException;
}
