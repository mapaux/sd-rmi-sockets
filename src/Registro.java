import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.net.*;
import java.io.*;

public class Registro {         
    public static void main (String args[])     
    {            
    	if (args.length < 2) {
			System.out.println("Debe indicar la direccion y la id donde registrar el invernadero");
			System.out.println("$./Registro direccion_registro id_registro");
			System.exit (1);
		}
    	
    	String URLRegistro;        
        String ruta = args[0];
        String id = args[1];
        
        try           
        {   
        	System.setSecurityManager(new RMISecurityManager());
        	ObjetoRemoto objetoRemoto = new ObjetoRemoto();                  
        	URLRegistro = "rmi://" + ruta +":1099/invernadero=" + id;
            Naming.rebind (URLRegistro, objetoRemoto);            
            
            //System.out.println("Datos");
            //System.out.println(objetoRemoto.getHumSensor());
            //System.out.println("");
            
            System.out.println("Servidor de objeto preparado.");
            //System.out.println(URLRegistro);
            //System.out.println("Ruta:");
            //System.out.println(ruta);
            //System.out.println("Invernadero:");
            //System.out.println(id);
        }            
        catch (Exception ex)            
        {                  
            System.out.println(ex);            
        }     
    }
}
