import java.*;

public class actuador {
	
	private int id;
	
	public actuador() 
	{
		this.id = 0;
	}
	
	public Sensor regularTemp(Sensor s){
		float dato = s.getValor();
		if(dato < 25){
			System.out.println("Aumentamos la temperatura");
			s.setValor(25);
		}
		else if(dato > 35){
			System.out.println("Reducimos la temperatura");
			s.setValor(35);
		}
		return s;
	}
	
	public Sensor regularHum(Sensor s){
		float dato = s.getValor();
		if(dato < 40){
			System.out.println("Activamos el goteo");
			s.setValor(40);
		}
		else if(dato > 75){
			System.out.println("Activamos el deshumidificador");
			s.setValor(75);
		}
		return s;
	}
	
}